ROOTVER="6.08.02-x86_64-slc6-gcc49-opt"
ENVDIR="${HOME}/Envs"
ENV="MVApython"
ENVPATH=${ENVDIR}/${ENV}
TMPPATH=${ENVPATH}/tmp
LOCALPACK="${ENVPATH}/lib/python2.7/site-packages"
TOPDIR=${PWD}
LCG_RELEASE="LCG_93 "

PACKAGES="numpy root_numpy scipy ruamel.yaml matplotlib"
if [ $MVA_GPU_MODE ]
then
    PACKAGES+=" tensorflow-gpu"
else
    PACKAGES+=" tensorflow"
fi

# install keras after tensorflow, standalone keras comes with better configurability
PACKAGES+=" keras"

## pip package install
function pinstall {
    # check if package exists, if not, install
    
    # quick check
    if [ -d ${LOCALPACK}/$1 ]; then
        return
    fi
    
    # robust check
    if [ $(pip show $1 | wc -c) -eq 0  ]; then 
        echo "  installing $1"
        pip install $1 --quiet
    fi 
}

## Start Setup
##========================================
echo "Setting up MVA development environment..."

## setup ROOT
if [ -z "$ROOTSYS" ]; then
    echo "  ROOT not configured"
    if command -v setupATLAS >/dev/null 2>&1; then 
        echo "  trying CVMFS..."
        setupATLAS --quiet
        lsetup "root ${ROOTVER}" --quiet --force
        lsetup git
        echo "  setup ROOT ${ROOTVER}"
    else
        echo "  CVMFS not available. Cannot setup ROOT. Aborting."
        return
    fi
fi

## create virtual environment 
if [ ! -d ${ENVPATH} ]; then
    echo "  virtual environment not found, installing..."
    #wget https://bootstrap.pypa.io/get-pip.py
    curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py --silent
    python get-pip.py --user --quiet
    python -m pip install virtualenv --user --quiet 
    export CC="gcc"
    mkdir -p ${ENVDIR}
    mkdir -p ${TMPPATH}
    export TMPDIR=$TMPPATH
    python -m virtualenv -p python ${ENVPATH} --quiet
    if [ -f get-pip.py ]; then
        rm get-pip.py
    fi
fi

## load environment
echo "  activating virtual env: ${ENV}"
source ${ENVPATH}/bin/activate

## install pip-compatible 3rd party packages
for package in ${PACKAGES}; do 
   pinstall ${package}
done

export MVAAREA=${PWD}
export PYTHONPATH=${MVAAREA}:${PYTHONPATH}

# creating alias shortcuts
alias NNAplicator="python ${MVAAREA}/python/NNAplicator"
alias NNTrainer="python ${MVAAREA}/python/NNTrainer"
alias NNPlotSummary="python ${MVAAREA}/python/NNPlotSummary"

echo "Setup complete!"
