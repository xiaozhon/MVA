import argparse, json
from core.factory import Worker

def runWorker(args):
    dWorkerData = {}
    with open(args.workerConf, "r") as json_file:
        dWorkerData = json.load(json_file)

    worker = Worker(**dWorkerData)
    worker.startRunning()
    worker.saveModel()


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("workerConf", type=str, help="worker con of worker that should be executed")

    args = parser.parse_args()
    runWorker(args)