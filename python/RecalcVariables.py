from core.utils.VarConfigExtractor import variables_to_dict
import argparse
import json, glob

def loadAuxInp(sAuxInpFile):
    with open(sAuxInpFile, "r") as inpAux:
        dAuxInp = json.load(inpAux)
    return dAuxInp

def correctVarData(sOutFolder, dAuxInp):
    with open(sOutFolder+"/model_variables.json") as oldVarFile:
        dVarData = json.load(oldVarFile)
    lNormData = dVarData["input_sequences"][0]["variables"]
    lNewNormData = []
    for dNormSet in lNormData:
        idx = dAuxInp["var"].index(dNormSet["name"])
        dNormSet["offset"] = -1.0 * dAuxInp["var_means"][idx]
        dNormSet["scale"] = 1.0 / dAuxInp["var_deviations"][idx]
        lNewNormData.append(dNormSet)

    dVarData["input_sequences"][0]["variables"] = lNewNormData

    return dVarData


def main(args):

    dInp = loadAuxInp(args.auxInp)
    
    if not dInp["is_normalized"]:
        return 

    for outFolder in args.outFolders:
        dCorrectedVar = correctVarData(outFolder, dInp)
        print dCorrectedVar

        with open(outFolder+"/model_variables_fixed.json", "w") as new_json_file:
            json.dump(dCorrectedVar, new_json_file, sort_keys=True, indent=2)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("auxInp", type=str)
    parser.add_argument("outFolders", nargs="+", type=str, default="./")

    args = parser.parse_args()

    main(args)