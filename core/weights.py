from core.base import BaseClass
from core.dataset import *
from core.labels import *
import matplotlib.pyplot as plt
import ROOT

import numpy as np
from numpy import inf

class WeightsBase(BaseClass):
    def __init__(self,
                 name):

        BaseClass.__init__(self, name)
        self.name = name

        self.data_train = None
        self.data_val = None


    def _convert(self):
        if not self.is_converted:
            self.data_train = self._build_weights("train")
            self.data_val = self._build_weights("val")

            self.is_converted = True


    def _build_weights(self, postfix=None):
        """Returns weights

        IMPORTANT: implementation must be provided by derived class.

        """
        log().warn("Class derived from WeightsBase must provide implementation of  _build_weights()!")


    def _check_labels_status(self, labelset):
        if not isinstance(labelset["object"], DatasetBase):
            log().error("No valid class to extract weights for {}!".format(self.name))
            return False
        return True

    def _save_divide(self, a, b):
        return np.divide(a, b, out=np.zeros_like(b).astype(float), where=b!=0)


class ClassWeightsBase(WeightsBase):
    # must there be a more generalized class holding regression labels?
    def __init__(self,
                name, 
                labelset,
                balance_daughters = False):

        WeightsBase.__init__(self, name)

        self.labelset = labelset
        self.balance_daughters = balance_daughters


    def _calc_weight_vector(self, labels_arr, lable_weights={}):
        if not isinstance(lable_weights, dict):
            log().error("Can not apply additional lable weights, format must be dict.")
            lable_weights={}

        dim = len(labels_arr.shape)

        labels_sum = None
        for _ in xrange(dim-1):
            if type(labels_sum) == type(None):
                labels_sum = labels_arr.sum(axis=0)
            else:
                labels_sum = labels_sum.sum(axis=0)

        weight_mean = labels_sum.mean()
        weight_vec = labels_sum/weight_mean
        weight_vec = self._save_divide(labels_sum.mean(), labels_sum)

        weight_vec_rec = []
        for w in weight_vec:
            if w > 30.: weight_vec_rec.append(30.)
            else: weight_vec_rec.append(w)

        for idx, w in lable_weights.iteritems():
            weight_vec_rec[idx] *= w
       
        return weight_mean, weight_vec_rec

class ClassBalance(ClassWeightsBase):

    def __init__(self,
                name): 
                #balance_daughters = False):

        #ClassWeightsBase.__init__(self, name, None, balance_daughters)
        ClassWeightsBase.__init__(self, name, None, False)
        self.labelset = None


    def __call__(self, labelset):
        if not isinstance(labelset, DatasetClassLables):
            log().error("Label set {0} from which class balance weights are derived in {1} must be {2}".format(labelset.name, self.name, DatasetClassLables))
            return None
        self.labelset = labelset
        return self


    def _get_labelsets_shape(self, postfix="train"):
        shapes = list()
        shapes = [getattr(self.labelset, "get_{}_data".format(postfix))().shape]

        return shapes


    def _check_labels_status(self, labelset):
        if not isinstance(labelset, DatasetClassLables):
            log().error("No valid class to extract weights for {}!".format(self.name))
            return False

        if not labelset.is_converted: # and not labelset.is_daughter():
            log().error("Label {0} used for weights {1} not yet converted! Hit convert() first!".format(self.labelset.name, self.name))
            return False

        return True


    def _build_weights(self, postfix="train"):
        if not self._check_labels_status(self.labelset):
            return None

        labels_arr = getattr( self.labelset, "get_{}_data".format(postfix) )()
        shapes = self._get_labelsets_shape(postfix)
        weights = None
        nEvents_lower_bound = 0
        nEvents_upper_bound = 0

        weight_means = list()
        weight_vectors = list()
        for shape in shapes:
            nEvents_upper_bound += shape[0]
            weight_mean, weight_vec = self._calc_weight_vector(labels_arr[nEvents_lower_bound:nEvents_upper_bound])
            nEvents_lower_bound += shape[0]
            weight_means.append(weight_mean)
            weight_vectors.append(weight_vec)
        
        nEvents_lower_bound = 0
        nEvents_upper_bound = 0
        rel_mean = np.array(weight_means).mean()
        
        for i in range(0, len(shapes)):
            nEvents_upper_bound += shapes[i][0]
            tmp_weights = labels_arr[nEvents_lower_bound:nEvents_upper_bound].dot(weight_vectors[i]) * (rel_mean/weight_means[i])
            nEvents_lower_bound += shapes[i][0]

            if type(weights) == type(None):
                weights = tmp_weights
            else:
                weights = np.append(weights, tmp_weights, axis=0)

        return weights

class VarWeight(ClassWeightsBase):
    def __init__(self,
                 name = None,
                 var = None,
                 normalize = False):

        ClassWeightsBase.__init__(self, name)
        self.name = name

        if not isinstance(var, str):
            log().error("Variable used for variable weighting {0} must be of type {1}".format(self.name, type(Var())))
        self.var = [var]
        self.normalize = normalize
        self.DS = None

    def __call__(self, copyObj):
        if not isinstance(copyObj, DatasetBase):
            log().error("Dataset from which class balance weights are derived in {0} must be of type {1}".format(self.name, DatasetBase))
            return None

        self.DS = self._construct_dataset(copyObj, "DS_"+self.name)
        if copyObj.is_parent():
            for daughter in copyObj.get_final_daughters():
                daughter_name = self.DS.name+"_from_"+daughter.name
                DSdaughter = self._construct_dataset(daughter, daughter_name)
                self.DS.add_daughter(DSdaughter)

        return self

    def _construct_dataset(self, copyObj, DSName):
        DS = Dataset(name=DSName, sample=copyObj.sample,var=self.var, sel=copyObj.sel, sel_train=copyObj.sel_train, sel_val=copyObj.sel_val, nevents_train=copyObj.nevents_train, nevents_val=copyObj.nevents_val)
        DS.initialize()
        return DS

    def _build_weights(self, postfix="train"):
        if not self.DS.is_converted:
            log().info("Start converting weight {}".format(self.name))
            self.DS.convert(self, True)

        weights = getattr(self.DS, "get_{}_data".format(postfix))()
        weights = np.concatenate(weights)
        if self.normalize:
            weights = weights*weights.shape[0]/weights.sum()
        return weights

class DatasetBalance(WeightsBase):
    def __init__(self,
                 name = None,
                 ds_keys = [],
                 ds_keys_weights = {}):

        WeightsBase.__init__(self, name)
        self.name = name
        self.ds_keys = ds_keys
        self.ds_keys_weights = ds_keys_weights
        self.DS = None

    def __call__(self, refObject):
        if not isinstance(refObject, DatasetBase):
            log().error("Dataset from which class balance weights are derived in {0} must be of type {1}".format(self.name, DatasetBase))
            return None

        self.DS = refObject
        return self

    def _build_weights(self, postfix="train"):
        if not self.DS.is_converted:
            log().warning("Dataset {} is not yet converted! Convert first".format(self.name))
            return

        nEvTotal = getattr(self.DS, "shape_"+postfix)[0]
        lSubEv = []

        lDsSelected = self.DS.get_final_daughters()
        if self.ds_keys:
            lDsSelected = self._get_ds_from_keys(self.DS, self.ds_keys)

        aveEv = 0
        for subDS in lDsSelected:
            addWeight = 1.
            for kName, kWeight in self.ds_keys_weights.iteritems():
                if subDS.name in kName: addWeight = float(kWeight)

            lSubEv.append( (getattr(subDS, "shape_"+postfix)[0], addWeight) )
            aveEv += float(lSubEv[-1][0])

        aveEv = aveEv/float(len(lDsSelected))

        weights = np.ones(nEvTotal)
        currentPos = 0
        for subEv, addWeight in lSubEv:
            weights[currentPos:currentPos+subEv] *= addWeight * aveEv/float(subEv)
            currentPos += subEv

        return weights

    def _get_ds_from_keys(self, ds, ds_keys, tree_lvl=True):
        lRet = []
        bKeyFound = False
        for dsName in ds_keys:
            if dsName in ds.name:
                lRet.append(ds)
                bKeyFound = True
            
        if not bKeyFound and ds.is_parent():
            for daughter in ds.daughters:
               lRet += self._get_ds_from_keys(daughter, ds_keys, False)
        elif not bKeyFound:
            log().error("Dataset {} not found! Can not generate lables for this!".format(dsName))
                
        return lRet


class MCWeight(ClassWeightsBase):
    def __init__(self,
                 name = None,
                 normalize = False,
                 lumi=1.0):

        ClassWeightsBase.__init__(self, name)
        self.name = name
        self.normalize = normalize
        self.lumi = lumi

    def __call__(self, data):
        if not isinstance(data, DatasetBase):
            log().error("Dataset from which class balance weights are derived in {0} must be of type {1}".format(self.name, DatasetBase))
            return None

        self.data = data
        return self

    def _build_weights(self, postfix="train"):
        weights = None
        for daughter in self.data.get_final_daughters():
            if not daughter.is_converted:
                log().error("Dataset {} not yet converted! Can not extract MC weights!".format(daughter.name))
                return None
            tree_caches = getattr(daughter,"tree_cache_"+postfix)
            for tree_cache in tree_caches:
                tmp_weights = np.ones(tree_cache["NumEventsPass"])*\
                                      tree_cache["XSec"]*\
                                      tree_cache["FilterEff"]*\
                                      tree_cache["KFactor"]/\
                                      tree_cache["NumEvents"]

                if not daughter.sample[0].is_data:
                    tmp_weights *= self.lumi

                if not isinstance(weights, np.ndarray):
                    weights = tmp_weights
                else:
                    weights = np.append(weights, tmp_weights)

        if self.normalize:
            weights = weights*weights.shape[0]/weights.sum()
        return weights



class RecurrentClassBalance(ClassWeightsBase):

    def __init__(self,
                name, 
                balance_daughters = False, 
                lable_weights = {}):

        ClassWeightsBase.__init__(self, name, None, balance_daughters)
        self.labelset = None
        self.lable_weights = lable_weights


    def __call__(self, labelset):
        if not type(labelset) == RecurrentClassLabels:
            log().error("Label set {0} from which class balance weights are derived in {1} must be {2}".format(labelset.name, self.name, type(RecurrentClassLabels)))
            return None
        self.labelset = labelset
        return self


    def _get_labelsets_shape(self, postfix="train"):
        shapes = list()
        if self.balance_daughters and self.labelset.DS.is_parent():
            for daughter in self.labelset.DS.get_final_daughters():
                shapes.append(getattr(daughter, "shape_"+postfix))
        else:
            shapes = [getattr(self.labelset, "get_{}_data".format(postfix))().shape]

        return shapes


    def _check_labels_status(self, labelset):
        if not isinstance(labelset, RecurrentClassLabels):
            log().error("No valid class to extract weights for {}!".format(self.name))
            return False

        if not labelset.is_converted:
            log().error("Label {0} used for weights {1} not yet converted! Hit convert() first!".format(self.labelset.name, self.name))
            return False

        return True


    def _build_weights(self, postfix="train"):
        if not self._check_labels_status(self.labelset):
            return None

        labels_arr = getattr( self.labelset, "get_{}_data".format(postfix) )()
        shapes = self._get_labelsets_shape(postfix)

        weights = None
        nEvents_lower_bound = 0
        nEvents_upper_bound = 0

        weight_means = list()
        weight_vectors = list()
        for shape in shapes:
            nEvents_upper_bound += shape[0]
            weight_mean, weight_vec = self._calc_weight_vector(labels_arr[nEvents_lower_bound:nEvents_upper_bound], lable_weights=self.lable_weights)
            nEvents_lower_bound += shape[0]
            weight_means.append(weight_mean)
            weight_vectors.append(weight_vec)

        nEvents_lower_bound = 0
        nEvents_upper_bound = 0
        rel_mean = np.array(weight_means).mean()
        
        for i in range(0, len(shapes)):
            nEvents_upper_bound += shapes[i][0]
            tmp_weights = labels_arr[nEvents_lower_bound:nEvents_upper_bound].dot(weight_vectors[i]) * (rel_mean/weight_means[i])
            nEvents_lower_bound += shapes[i][0]

            if type(weights) == type(None):
                weights = tmp_weights
            else:
                weights = np.append(weights, tmp_weights, axis=0)

        return weights


class RecurrentVarBalance(WeightsBase):
    
    def __init__(self,
                 name,
                 var):

        WeightsBase.__init__(self, name)
        
        if not isinstance(var, str):
            log().error("Variable used for variable weighting {0} must be of type {1}".format(self.name, type(Var())))
        self.var = [var]
        self.DS = None


    def __call__(self, copyObj):
        if not isinstance(copyObj, RecurrentDataset):
            log().error("Dataset from which class balance weights are derived in {0} must be of type {1}".format(self.name, RecurrentDataset))
            return None

        self.DS = self._construct_dataset(copyObj, "DS_"+self.name)
        if copyObj.is_parent():
            for daughter in copyObj.get_final_daughters():
                daughter_name = self.DS.name+"_from_"+daughter.name
                self.DS.add_daughter(self._construct_dataset(daughter, daughter_name))

        return self

    def _construct_dataset(self, copyObj, DSName):
        DS = RecurrentDataset(name=DSName, sample=copyObj.sample,var=self.var, rec_steps=copyObj.rec_steps, sel=copyObj.sel, sel_train=copyObj.sel_train, sel_val=copyObj.sel_val, nevents_train=copyObj.nevents_train, nevents_val=copyObj.nevents_val)
        DS.initialize()
        return DS

    def _get_labelsets_shape(self, postfix="train"):
        shapes = list()
        if self.DS.is_parent():
            for daughter in self.DS.get_final_daughters():
                shapes.append(getattr(daughter, "shape_"+postfix))
        else:
            shapes = [getattr(self.DS, "get_{}_data".format(postfix))().shape]

        return shapes


    def _build_weights(self, postfix=None, idx=0):
        if not self.DS.is_converted:
            self.DS.convert(True)

        import matplotlib.pyplot as plt
        var_data = getattr(self.DS , "get_{}_data".format(postfix))().copy()
        var_dist = np.hstack(var_data[:,0,:])
        shapes = self._get_labelsets_shape(postfix)
        
        nEvents_lower_bound = 0
        nEvents_upper_bound = 0
        
        hist_data_list = list()
        borders = "sqrt"
        for shape in shapes:
            nEvents_upper_bound += shape[0]
            tmp_hist_data, borders = np.histogram(var_dist[nEvents_lower_bound:nEvents_upper_bound], bins=borders)
            nEvents_lower_bound += shape[0]
            hist_data_list.append(tmp_hist_data.astype(float).copy())

        if idx > len(hist_data_list):
            log().warn("Dataset refference index larger than number of datasets to balance! Use fist dataset as refference!")
            idx = 0

        lower_borders = borders[:-1]
        #upper_borders = borders[1:]

        var_mask = np.digitize(var_data, lower_borders)

        var_weights = None
        nEvents_lower_bound = 0
        nEvents_upper_bound = 0
        
        for i in range(0, len(shapes)):
            weight_vec = self._save_divide(hist_data_list[idx], hist_data_list[i])
            nEvents_upper_bound += shapes[i][0]
            local_mask = var_mask[nEvents_lower_bound:nEvents_upper_bound]
            local_mask = local_mask.reshape(local_mask.shape[0], local_mask.shape[1])
            tmp_weights = (weight_vec[...])[local_mask-1]
            
            nEvents_lower_bound += shapes[i][0]
            
            if type(var_weights) == type(None):
                var_weights = tmp_weights
            else:
                var_weights = np.append(var_weights, tmp_weights, axis=0)

        return var_weights