import subprocess, sys, time, os
from core.utils.logger import log


class DirectDriver:

    def __init__(self):
        self.lWorkers = []

    def setWorkers(self, lWorkers):
        self.lWorkers = lWorkers

    def submit(self):
        for worker in self.lWorkers:
            worker.startRunning()
            worker.saveModel()

class ParallelDriver(DirectDriver):

    def __init__(self):
        DirectDriver.__init__(self)

    def submit(self):
        lWorkerConfFiles = [worker.getWorkerConfFile() for worker in self.lWorkers]
        command = "nice -n 19 parallel python python/RunWorker.py {0} ::: "+" ".join(lWorkerConfFiles)
        sub_process = subprocess.Popen(command, close_fds=True, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        log().info("Job submitted using parallel driver")
        log().info("Job executed using the following command:")
        log().info("================================================================================")
        print(command)
        log().info("================================================================================")
        log().info("Check every 60 seconds if jobs are done")
        while sub_process.poll() is None:
            time.sleep(60)
            log().info("Jobs still running. Check again in 60 seconds.")
        log().info("Jobs done")


class SlurmDriver(DirectDriver):

    def __init__(self):
        DirectDriver.__init__(self)

    def submit(self):
        lWorkerConfFiles = [worker.getWorkerConfFile() for worker in self.lWorkers]

        log().info("Job submitted using slurm driver")
        log().info("Submitting job array")
        jobFile, runFolder = self._createJobFile(lWorkerConfFiles)
        command = "sbatch --array=0-{} -D {} {}".format(len(lWorkerConfFiles)-1, runFolder, jobFile)
        sub_process = subprocess.Popen(command, close_fds=True, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        log().info("Job submission done")


    def _createJobFile(self, lWorkerConfFiles):
        outFolder = self.lWorkers[0].outFolder
        runFolder = outFolder+"/submit"
        jofFileName = runFolder+"/slurm_job_file.sh"
        
        BaseDir = os.getenv("MVAAREA")
        
        if not os.path.isdir(runFolder):
            os.mkdir(runFolder)

        with open(jofFileName, "w") as jobFile:
            jobFile
            jobFile.write("#!/bin/bash\n")
            jobFile.write("#SBATCH --time=02:30:00\n")
            jobFile.write("#SBATCH --cpus-per-task=3\n")
            jobFile.write("#SBATCH --mem-per-cpu=2500\n")
            #jobFile.write("#SBATCH --partition=haswell,sandy\n")
            jobFile.write("#SBATCH --gres=gpu:1\n")

            jobFile.write("WORKER_CONFS='"+" ".join(lWorkerConfFiles)+"'\n")
            jobFile.write("WORKER_CONFS_ARRAY=($WORKER_CONFS)\n")
            jobFile.write("CURRENT_CONF=${WORKER_CONFS_ARRAY[${SLURM_ARRAY_TASK_ID}]}\n")

            jobFile.write("cd "+BaseDir+"\n")
            jobFile.write("source setup_taurus.sh\n")
            
            jobFile.write("cd "+runFolder+"\n")
            jobFile.write("echo $CURRENT_CONF\n")
            jobFile.write("echo $SLURM_JOB_PARTITION\n")
            jobFile.write("python "+BaseDir+"/python/RunWorker.py $CURRENT_CONF\n")

        return jofFileName, runFolder

