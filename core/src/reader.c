#include "TTree.h"
#include "TTreeFormula.h"
#include <iostream>

double_t * test_array()
{
  double_t * test = new double_t [10];
  
  for(int i=0; i<10; ++i){
    for(int j=0; j<4; ++j){
      test[i]=i*j ;
    }  
  }
  
  return test;
  
}

unsigned long long get_nevents(TTree* t_tree, std::string s_formula)
{
  TTreeFormula * testFormula = new TTreeFormula("testFormula", s_formula.c_str(), t_tree);
  
  unsigned long long max_events = 0.0;
  
  std::cout<<"start looping"<<std::endl;
  std::cout<<s_formula<<std::endl;
  for(long long i=0; i<t_tree->GetEntries(); ++i)
  {
    t_tree->GetEntry(i);
    std::cout<<i<<std::endl;
    //std::cout<<testFormula->GetNdata()<<std::endl;
  }
  std::cout<<"end looping"<<std::endl;
  //delete testFormula;
  
  return 0.0;
} 
