from dataset import *
from core.base import BaseClass
import numpy as np

class RecurrentClassLabels(BaseClass):

    def __init__(self,
                name = None,
                var = None,
                nclasses = None
                ):
        
        BaseClass.__init__(self, name)

        if not isinstance(var, str):
            log().error("Variable used for variable balancing {0} must be of type string".format(name))
        
        self.var = [var]
        self.nclasses = nclasses

        self.mask_train = None
        self.mask_val = None

        self.DS = None


    def __call__(self, copyObj):
        if isinstance(copyObj, RecurrentDataset):
            if not copyObj.is_converted and not copyObj.is_daughter():
                log().error("Cant invoke copy constructor for {} on object that is not converted!".format(self.name))
                return None

            if copyObj.is_parent():
                self.DS = self._construct_parent(copyObj)

            else:
                self.DS = self._construct_daughter(copyObj)
            return self
        else:
            log().error("Cant invoke copy constructor for {} on object of different base type!".format(self.name))
            return None


    def _construct_parent(self, copyObj):
        DS = RecurrentDataset("DS_"+self.name)
        
        for copyDaughter in copyObj.get_final_daughters():
            DS.add_daughter(self._construct_daughter(copyDaughter, "DS_"+self.name+"_from_"+copyDaughter.name))
        DS.initialize()

        self.mask_train = DS.get_mask(copyObj.data_train)
        self.mask_val = DS.get_mask(copyObj.data_val)

        self.tree_cache_train = copyObj.tree_cache_train
        self.tree_cache_val = copyObj.tree_cache_val

        return DS


    def _construct_daughter(self, copyObj, copyName = ""):
        nevents_train = copyObj.nevents_train
        nevents_val = copyObj.nevents_val
        sel = copyObj.sel
        sel_train = copyObj.sel_train
        sel_val = copyObj.sel_val
        rec_steps = copyObj.rec_steps
        
        sample = copyObj.sample
        shape_train = copyObj.shape_train
        shape_val = copyObj.shape_val

        name = "DS_"+self.name
        if copyName:
            name = copyName
        DS = RecurrentDataset(name=name, var=self.var, sample=sample, rec_steps=rec_steps, nevents_train=nevents_train, nevents_val=nevents_val, sel=sel, sel_train=sel_train, sel_val=sel_val)
        DS.initialize()

        if not copyObj.is_daughter():
            self.mask_train = self.get_mask(copyObj.data_train)
            self.mask_val = self.get_mask(copyObj.data_val)

        return DS


    def _normalize(self):
        log().warn("Class datasets doesnt have to be normalized! Invoke <encode> to get labels.")


    def _convert(self):
        if not self.DS.is_converted:
            self.DS.convert(True)

        self.set_class_labels()
        self.data_train =  self.to_onehot(self.DS.data_train)
        self.data_val =  self.to_onehot(self.DS.data_val)

        if type(self.mask_train) != type(None) and type(self.mask_val) != type(None):
            self.data_train =  (self.data_train.T*self.mask_train.T).T 
            self.data_val =  (self.data_val.T*self.mask_val.T).T
        self.is_converted = True


    def set_class_labels(self):
        if not self.nclasses:
            self.nclasses = np.max(self.DS.data_train)+1
            log().info("Automatically detected {} classes for label set {}".format(self.nclasses, self.name))


    def to_onehot(self, a):
        # this abuses broadcasting
        return (np.arange(self.nclasses) == a[...]).astype(int)


class DatasetClassLables(BaseClass):

    def __init__(self,
                 name = None,
                 dataset_keys = None,
                 do_binary = False):
        
        BaseClass.__init__(self, name)
        self.name = name
        self.dataset_keys = dataset_keys
        if len(self.dataset_keys) > 2 and do_binary:
            log().warning("Label {} can not be binary with more then 2 dataset keys".format(self.name))
            do_binary = False

        self.do_binary = do_binary
        self.data_train = None
        self.data_val = None
        self.is_converted = False

    def __call__(self, obj):
        if not isinstance(obj, DatasetBase):
            log().error("Cant invoke {} on object of different base type then {}!".format(self.name, type(DatasetBase())))
            return None

        if not obj.is_converted:
            log().error("Cant invoke {} on dataset {} since dataset is not yet converted!".format(self.name, obj.name))
            return None

        self.dataset = obj
        return self

    def _convert(self):
        log().info("Start encoding dataset class labes for {}".format(self.name))
        lDSselected = self._get_ds_from_keys(self.dataset, self.dataset_keys)

        lSizeTrain = [ds.shape_train[0] for ds in lDSselected]
        lSizeVal = [ds.shape_val[0] for ds in lDSselected]
        self.data_train = self._generate_labels(lSizeTrain)
        self.data_val = self._generate_labels(lSizeVal)
        self.is_converted = True

    def _generate_labels(self, lSizes):
        aLables = None
        for idx, size in enumerate(lSizes):
            if not isinstance(aLables, np.ndarray):
                aLables = np.ones(size) * idx
            else:
                aLables = np.append(aLables, np.ones(size) * idx)

        if not self.do_binary:
            aLables = self.to_onehot(aLables, len(lSizes))

        #if self.do_binary:
        #    aLables = aLables[:,0]
        return aLables


    def _get_ds_from_keys(self, ds, ds_keys, tree_lvl=True):
        lRet = []
        bKeyFound = False
        for dsName in ds_keys:
            if dsName in ds.name:
                lRet.append(ds)
                bKeyFound = True
            
        if not bKeyFound and ds.is_parent():
            for daughter in ds.daughters:
               lRet += self._get_ds_from_keys(daughter, ds_keys, False)
        elif not bKeyFound:
            log().error("Dataset {} not found! Can not generate lables for this!".format(dsName))
                
        return lRet

    def to_onehot(self, a, nclasses):
        # this abuses broadcasting
        return (np.arange(nclasses) == a[:,None]).astype(int)