import numpy as np
from core.utils.logger import log
import json

import os


class BaseClass(object):

    def __init__(self, name):

        self.name = name

        self.is_converted = False

        # internal data config and holders
        self.name_train = self.name.replace(" ", "_")+"_train"
        self.data_train = None
        self.shape_train = None
        self.persistent_file_train = None

        self.name_val   = self.name.replace(" ", "_")+"_val"
        self.data_val   = None
        self.shape_val   = None
        self.persistent_file_val = None

        self.name_test   = self.name.replace(" ", "_")+"_test"
        self.data_test   = None
        self.shape_test   = None
        self.persistent_file_test = None

    # _________________________________________________________________________________________________
    def initialize(self):
        pass

    # _________________________________________________________________________________________________
    def get_train_data(self):
        if not self.is_converted:
            log().error("Cant load train dataset {} since it is not yet converted!".format(self.name))
            return None

        if isinstance(self.data_train, np.ndarray):
            return self.data_train
        elif self.is_persistent:
            return np.load(self.persistent_file_train)
        else:
            log().error("Cant load train dataset {}!".format(self.name))
            log().error("Dataset is not persistent on disk or in memory")
            return None

# _________________________________________________________________________________________________
    def get_val_data(self):
        if not self.is_converted:
            log().error("Cant load validation dataset {} since it is not yet converted!".format(self.name))
            return None

        if isinstance(self.data_val, np.ndarray):
            return self.data_val
        elif self.is_persistent:
            return np.load(self.persistent_file_val)
        else:
            log().error("Cant load validation dataset {}!".format(self.name))
            log().error("Dataset is not persistent on disk or in memory")
            return None
    
    # _________________________________________________________________________________________________
    def _auxData(self):
        return {}

    # _________________________________________________________________________________________________
    def persistify(self, outFolder, auxOnly=False):
        if not self.is_converted:
            log().warning("Cant persistify dataset since it is not yet converted!")
            return

        if not outFolder.endswith("/"):
            outFolder += "/"

        with open(outFolder+self.name+".aux", "w") as auxFile:
            json.dump(self._auxData(), auxFile, sort_keys=True, indent=2, cls=NumpyEncoder)

        if auxOnly: return

        self.persistent_file_train = outFolder+self.name_train+".npy"
        self.persistent_file_val = outFolder+self.name_val+".npy"

        np.save(self.persistent_file_train, self.get_train_data())
        np.save(self.persistent_file_val, self.get_val_data())

        self.is_persistent = True

    # _________________________________________________________________________________________________
    def link_data(self, outFolder, auxOnly=False):
        if not outFolder.endswith("/"):
            outFolder += "/"

        with open(outFolder+self.name+".aux", "r") as auxFile:
            dAuxData = json.load(auxFile)
            for k, val in dAuxData.iteritems():
                setattr(self, k, val)

        if auxOnly: return

        if not os.path.isfile(outFolder+self.name_train+".npy") or not os.path.isfile(outFolder+self.name_val+".npy"):
            log().error("Can not link dataset {} to train/validation output. File does not exists!".format(self.name))
            raise Exception("File error")

        self.persistent_file_train = outFolder+self.name_train+".npy"
        self.persistent_file_val = outFolder+self.name_val+".npy"

        self.is_converted = True
        self.is_persistent = True

    # _________________________________________________________________________________________________
    def _convert(self):
        """Convert TTree to HDF5 format

        IMPORTANT: implementation must be provided by derived class.

        :param output: output folder to dump HDF5 file
        :type output: str
        """
        log().warn("Class derived from DatasetBase must provide implementation of _convert()!")
        return None

    # _________________________________________________________________________________________________
    def convert(self, quiet=False):
        if not quiet:
            log().info("Start converting dataset {}".format(self.name))

        self._convert()

# thanks to https://github.com/mpld3/mpld3/issues/434#issuecomment-340255689
class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """
    def default(self, obj):
        if isinstance(obj, (np.int_, np.intc, np.intp, np.int8,
            np.int16, np.int32, np.int64, np.uint8,
            np.uint16, np.uint32, np.uint64)):
            return int(obj)
        elif isinstance(obj, (np.float_, np.float16, np.float32, 
            np.float64)):
            return float(obj)
        elif isinstance(obj,(np.ndarray,)): #### This is the fix
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)