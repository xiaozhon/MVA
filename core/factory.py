from core.base import NumpyEncoder
from core.utils.ConfigHandler import ConfigHandler
from core.utils.logger import log
from core.drivers import DirectDriver, ParallelDriver, SlurmDriver

import os, imp, glob
from ruamel import yaml
import json

from datetime import datetime
import inspect


class NNfactory:

    def __init__(self):
        self.JOFile = ""
        self.inpFolder = ''
        self.outFolder = ''
        self.jobConfFile = ''
        self.mcWeightsFile = ''
        self.mergeConfFile = ''

        self.doKFolds = False
        self.kFolds = "10:1:1"

        self.dJobConf = None
        self.ConfigHandler = None
        self.JOmodule = None
        pass

    def setInpFolder(self, inpFolder):
        self.inpFolder = inpFolder

    def setOutFolder(self, outFolder):
        now = datetime.now()
        addTime = now.strftime("%Y-%m-%d_%H-%M-%S")
        self.outFolder = outFolder + "NNoutput_" + addTime + "/"

    def setJOFile(self, JOFile):
        self.JOFile = os.path.abspath(JOFile)

    def setMCWeightsFile(self, mcWeightsFile):
        self.mcWeightsFile = os.path.abspath(mcWeightsFile)

    def setMergConfFile(self, mergeConfFile):
        self.mergeConfFile = os.path.abspath(mergeConfFile)

    def setKFolds(self, doKFolds, kFolds):
        self.doKFolds = doKFolds
        self.kFolds = kFolds

    def parseJobConfig(self, jobConfFile):
        self.jobConfFile = os.path.abspath(jobConfFile)
        self.dJobConf = self.__loadConf()

    def createOutputFolder(self):
        if os.path.isdir(self.outFolder):
            log().error("Output {} already existing!".format(self.outFolder))
            raise Exception("Output already exists")

        os.mkdir(self.outFolder)
        os.mkdir(self.outFolder+"data")
        os.mkdir(self.outFolder+"model")
        os.mkdir(self.outFolder+"worker")

    def startRunning(self, driver="direct"):
        self.lWorkers = self.__createWorkers()

        lUniqueWorker = []
        for worker in self.lWorkers:
            if not worker.getAddName(True) in [uw.getAddName(True) for uw in lUniqueWorker]:
                lUniqueWorker.append(worker)

        for worker in lUniqueWorker: 
            worker.startConvertion()

        for worker in self.lWorkers:
            worker.saveToDisk()

        if driver=="slurm":
            driver = SlurmDriver()
        elif driver=="parallel":
            driver = ParallelDriver()
        elif driver=="direct":
            driver = DirectDriver()
        else:
            log().warning("Don't know dirver config {}. Please use one of [direct, parallel, slurm].")
            log().warning("Use direct driver as default.")
            driver = DirectDriver()
            
        driver.setWorkers(self.lWorkers)
        driver.submit()
        

    def __loadConf(self):
        stream = open(self.jobConfFile)
        return yaml.load(stream, Loader=yaml.Loader)

    def __createWorkers(self):
        dVarConf = self.dJobConf["Variables"]            
        dSampleConf = self.dJobConf["Samples"]
        dDSConf = self.dJobConf["Datasets"]
        dNNConf = self.dJobConf["Network"]
        dFitConf = self.dJobConf["Fit"]

        lWorkers = []
        lDSResolvedConf = []
        for DSName, dConf in dDSConf.iteritems():
            lTmpResolvedConf = []
            if not "FromDS" in dConf:
                lTmpResolvedConf = self.__getResolvedConf(dConf)
            else:
                lTmpResolvedConf = self.__getResolvedConf(dDSConf[dConf["FromDS"]])

            if lDSResolvedConf:
                if not len(lDSResolvedConf) == len(lTmpResolvedConf):
                    log().error("Number of configured dataset configurations for dataset {} differ from previous dataset configurations!".format(DSName))
                    log().error("Check that all dataset have the same number of resolved configurations!")
                    log().error("\t# of configs for {}: {}".format(DSName, len(lTmpResolvedConf)))
                    log().error("\t# of previouse DS configs: {}".format(len(lDSResolvedConf)))
                    raise Exception("Different DS config lengths")

                for i in range(0, len(lTmpResolvedConf)):
                    dTmpConf = lTmpResolvedConf[i]
                    lDSResolvedConf[i][DSName] = dTmpConf
            else:
                lDSResolvedConf += [{DSName : subConf} for subConf in lTmpResolvedConf]

        for dNNResolvedConf in self.__getResolvedConf(dNNConf):
            for dFitResolvedConf in self.__getResolvedConf(dFitConf):
                for dDSResolvedConf in lDSResolvedConf:
                    for kFoldConf, doKFolds in self.__getKFoldConf():
                        wk = Worker(workerName = "worker{}".format(len(lWorkers)),
                                    inpFolder = self.inpFolder, 
                                    outFolder = self.outFolder,
                                    mergeConfFile = self.mergeConfFile,
                                    mcWeightsFile = self.mcWeightsFile,
                                    JOFile = self.JOFile,
                                    dDSConf = dDSResolvedConf,
                                    dSampleConf = dSampleConf,
                                    dVarConf = dVarConf,
                                    dNNConf = dNNResolvedConf,
                                    dFitConf = dFitResolvedConf,
                                    doKFolds = doKFolds,
                                    kFoldConf = kFoldConf)
                        lWorkers.append(wk)

        log().info("Successfully added {} of workers".format(len(lWorkers)))
        return lWorkers

    def __getResolvedConf(self, dConf, addName=""):
        lConfs = []

        ## Create cartesian product of list configs
        isResolved = True
        for conf, val in dConf.iteritems():
            if isinstance(val, list):
                for subVal in val:
                    isResolved = False
                    dCopyConf = self.__deepCopyConf(dConf)
                    dCopyConf[conf] = subVal
                    subAddName = "_{}-{}".format(conf, subVal)

                    if subAddName > addName:
                        subAddName = addName + subAddName
                    else:
                        subAddName = subAddName + addName 

                    dCopyConf["addName"] = subAddName
                    lConfs += self.__getResolvedConf(dCopyConf, subAddName)
        
        if isResolved:
            lConfs.append(self.__deepCopyConf(dConf))

        # return only unique list of configs, naybe rework this section
        lRetConfs = []
        for conf in lConfs:
            isUnique = True
            for retConf in lRetConfs:
                isCopy = True
                for key, val in retConf.iteritems():
                    if key == 'addName' : continue
                    if conf[key] != val:
                        isCopy = False
                if isCopy:
                    isUnique = False

            if isUnique or (not lRetConfs):
                lRetConfs.append(conf)


        return lRetConfs

    def __getKFoldConf(self):
        if not self.doKFolds:
            return [("" , False)]
        
        lKFoldConf = []
        iFolds = int(self.kFolds.split(":")[0])
        iFoldsVal = int(self.kFolds.split(":")[1])
        iFoldsTest = int(self.kFolds.split(":")[2])
        iFoldsTrain = iFolds - iFoldsVal - iFoldsTest - 1
        for i in range(0, iFolds):
            lKFoldConf.append( ("{}:{}-{}:{}-{}:{}-{}".format(iFolds, i, 
                                                              (iFoldsTrain+i)%iFolds, 
                                                              (iFoldsTrain+i+1)%iFolds, 
                                                              (iFoldsTrain+i+iFoldsVal)%iFolds,
                                                              (iFoldsTrain+i+iFoldsVal+1)%iFolds,
                                                              (iFoldsTrain+i+iFoldsVal+iFoldsTest)%iFolds)
                                                              , True) )

        return lKFoldConf


    def __deepCopyConf(self, dConf):
        dCopyConf = {}
        for key, val in dConf.iteritems():
            dCopyConf[key] = val
        return dCopyConf

class Worker:
    def __init__(self,
                 workerName = "",
                 inpFolder = "",
                 outFolder = "",
                 mergeConfFile = "",
                 mcWeightsFile = "",

                 JOFile = None,
                 dDSConf = None,
                 dSampleConf = None,
                 dVarConf = None,
                 dNNConf = None,
                 dFitConf = None,

                 doKFolds = False,
                 # config used to split samples <kFolds tot>:<folds train>:<folds val>:<folds test>
                 kFoldConf = "10:0-7:8-8:9-9"):


        _, _, _, self.saveValues = inspect.getargvalues(inspect.currentframe()) #This is needed to sabe workers to disk for recreation
        self.saveValues.pop("self")

        self.workerName = workerName

        self.inpFolder = inpFolder
        self.outFolder = outFolder
        self.mergeConfFile = mergeConfFile
        self.mcWeightsFile = mcWeightsFile
    
        self.JOFile = JOFile
        self.dDSConf = dDSConf
        self.dSampleConf = dSampleConf
        self.dVarConf = dVarConf
        self.dNNConf = dNNConf
        self.dFitConf = dFitConf

        self.doKFolds = doKFolds
        self.kFoldConf = kFoldConf

        self.is_converted = False

        self.workerName = self.workerName + self.getAddName()
        self.JO = self.__parseJO()
        self.CH = self.__configureConfHandler()
        self.buildOutput()

    def buildOutput(self):
        if not os.path.isdir(self.getDataOutput()):
            os.mkdir(self.getDataOutput())
        if not os.path.isdir(self.getModelOutput()):
            os.mkdir(self.getModelOutput())
        if not os.path.isdir(self.getWorkerOutput()):
            os.mkdir(self.getWorkerOutput())

    def getAddName(self, DSonly=False):
        addName = ""
        
        def formatStr(s):
            s = s.replace("(", "").replace(")", "").replace("$", "").replace(" ", "")
            s = s.replace("/", "DIV").replace("\\", "DIV").replace("%", "MOD")
            s = s.replace("==", "EQ").replace("=", "EQ").replace("!=", "NEQ").replace(">", "GR").replace("<", "L")
            return s

        if "addName" in self.dDSConf.values()[0]:
            addName += formatStr(self.dDSConf.values()[0]["addName"])
        
        if DSonly:
            if self.doKFolds: addName += self.getKfoldName()
            if addName == "": addName = "_nominal"
            return addName

        if "addName" in self.dNNConf :
            addName += formatStr(self.dNNConf["addName"])
        if "addName" in self.dFitConf :
            addName += formatStr(self.dFitConf["addName"])

        if addName == "": addName = "_nominal"

        return addName

    def getKfoldName(self):
        addName = ""
        if self.doKFolds:
            addName = "_kFold_{}".format(self.kFoldConf)
        return addName


    def getDataOutput(self):
        return self.outFolder + "data/data" + self.getAddName(True)

    def getModelOutput(self):
        return self.outFolder + "model/model" + self.getAddName()

    def getWorkerOutput(self):
        return self.outFolder + "worker/worker" + self.getAddName()

    def getWorkerConfFile(self):
        return self.getWorkerOutput()+"/worker_conf{}.json".format(self.getKfoldName())


    def __configureConfHandler(self):
        CH = ConfigHandler()
        CH.inpFolder = self.inpFolder 
        CH.mergeConfFile = self.mergeConfFile
        CH.mcWeightsFile = self.mcWeightsFile

        CH.modelOutput = self.getModelOutput()

        CH.dDSConf = self.dDSConf 
        CH.dSampleConf = self.dSampleConf 
        CH.dVarConf = self.dVarConf 
        CH.dNNConf = self.dNNConf 
        CH.dFitConf = self.dFitConf 

        CH.kFoldConf = self.kFoldConf

        return CH

    def __parseJO(self):
        JO = imp.load_source('*', self.JOFile)

        return JO

    def getKFoldCut(self, mode="train"):
        lConfs = self.kFoldConf.split(":")
        iSliceTot = int(lConfs[0])

        idx = None
        if mode == "train":
            idx = 1
        elif mode == "val":
            idx = 2
        elif mode == "test":
            idx = 3
        else:
            log().error("Worker can not calculate k-Fold split for mode {}!".format(mode))
            log().error("Only ever use train, val or test")

        iSliceDown = int(lConfs[idx].split("-")[0])
        iSliceUp = int(lConfs[idx].split("-")[1])

        sCut = ""
        if iSliceUp > iSliceDown:
            sCut="((Entry$%{0}>={1}) && (Entry$%{0}<={2}))".format(iSliceTot, iSliceDown, iSliceUp)
        elif iSliceUp < iSliceDown:
            sCut="((Entry$%{0}>={1}) || (Entry$%{0}<={2}))".format(iSliceTot, iSliceDown, iSliceUp)
        else:
            sCut="((Entry$%{0}=={1}))".format(iSliceTot, iSliceDown)

        return sCut


    def startConvertion(self):
        self.JO.defineSamples(self.CH)
        for DSName, DS in self.CH.dDatasets.iteritems():
            if self.doKFolds and not DS.is_parent():
                DS.sel_train += "*"+self.getKFoldCut()
                DS.sel_val += "*"+self.getKFoldCut("val")
                DS.sel_test += "*"+self.getKFoldCut("test")

        for DSName, DS in self.CH.dDatasets.iteritems():
            if "Convert" in self.dDSConf[DSName]:
                if not self.dDSConf[DSName]["Convert"] : continue

            DS.convert()

            if "Normalize" in self.dDSConf[DSName]:
                if self.dDSConf[DSName]["Normalize"] : DS.normalize()

            DS.persistify(self.getDataOutput())

        self.JO.defineLabels(self.CH)
        for LabelName, Label in self.CH.dLabels.iteritems():
            Label.convert()
            Label.persistify(self.getDataOutput())

        self.JO.defineWeights(self.CH)
        for WeightName, Weight in self.CH.dWeights.iteritems():
            Weight.convert()
            Weight.persistify(self.getDataOutput())

        self.is_converted = True

    def startRunning(self):
        if not self.is_converted:
            self._startRelinking()

        self.JO.defineModel(self.CH)
        self.JO.fitModel(self.CH)

    def saveModel(self):
        if self.CH.model:
            self.CH.model.save(self.getModelOutput()+"/model{}.h5".format(self.getKfoldName()))
            self.CH.model.save_weights(self.getModelOutput()+"/model_weights{}.h5".format(self.getKfoldName()))
            dModel = json.loads(self.CH.model.to_json())
            with open(self.getModelOutput()+"/model_conf{}.json".format(self.getKfoldName()), "w") as json_file:
                json.dump(dModel, json_file, sort_keys=True, indent=2)

        if self.CH.history:
            with open(self.getModelOutput()+"/fit_history{}.json".format(self.getKfoldName()), "w") as json_file:
                json.dump(self.CH.history.history, json_file, sort_keys=True, indent=2, cls=NumpyEncoder)

        if self.CH.varDict:
            with open(self.getModelOutput()+"/model_variables{}.json".format(self.getKfoldName()), "w") as json_file:
                json.dump(self.CH.varDict, json_file, sort_keys=True, indent=2)


    def saveToDisk(self):
        with open(self.getWorkerConfFile(), "w") as json_file:
            json.dump(self.saveValues, json_file, sort_keys=True, indent=2)

    def _startRelinking(self):
        self.JO.defineSamples(self.CH)
        for DSName, DS in self.CH.dDatasets.iteritems():
            if "Convert" in self.dDSConf[DSName]:
                if not self.dDSConf[DSName]["Convert"] : continue

            DS.link_data(self.getDataOutput())

        self.JO.defineLabels(self.CH)
        for LabelName, Label in self.CH.dLabels.iteritems():
            Label.link_data(self.getDataOutput())

        self.JO.defineWeights(self.CH)
        for WeightName, Weight in self.CH.dWeights.iteritems():
            Weight.link_data(self.getDataOutput())

        self.is_converted = True

    def reloadModel(self, epoch=0):
        from keras.models import model_from_json
        modelConf = ""
        with open( self.getModelOutput()+"/model_conf{}.json".format(self.getKfoldName()) ) as json_file:
            for line in json_file:
                modelConf += " "+line.replace("\n", "")

        model = model_from_json(modelConf)

        weightFileName = self.getModelOutput()+"/model_weights{}.h5".format(self.getKfoldName())
        if epoch:
            weightFileName = glob.glob( self.getModelOutput()+"/model_weights.{}.{}-*.hdf5".format(self.getKfoldName()[1:], int(epoch)) )[0]

        log().info("Load weights from weight file: "+weightFileName.split("/")[-1])
        model.load_weights(weightFileName)
        self.CH.setModel(model)


    def isUsedInTraining(self, fileName):
        if not self.is_converted:
            self._startRelinking()

        is_train = False
        for DSName, DS in self.CH.dDatasets.iteritems():
            if DS.is_spectator: continue
            if DS.is_parent():
                for daughter in DS.get_final_daughters():
                    if daughter.is_file_in_sample(fileName): is_train = True
            else:
                if DS.is_file_in_sample(fileName): is_train = True
        return is_train


    def getPrediction(self, fileName, tree):
        if not self.is_converted:
            self._startRelinking()

        dSelCache = {}
        for DSName, DS in self.CH.dDatasets.iteritems():
            if DS.is_spectator: continue
            # set train/val/test splitting only for none parent when sapmes is used for training
            # cache previous selection setup to reset the sel after we are done
            dSelCache[DSName] = {}
            if self.doKFolds and self.isUsedInTraining(fileName):
                for part in ["train", "val", "test"]:
                    dSelCache[DSName][part] = getattr(DS, "sel_"+part)
                    setattr(DS, "sel_"+part, dSelCache[DSName][part]+"*"+self.getKFoldCut(part))

            # now setting up sample to predict on using datasets:
            #####################################################
            # do not setup dougthers individually but rather parents only
            DS.set_application_mode(True)
            DS.set_application_tree(tree)
            #if DS.is_daughter(): continue
            #elif DS.is_parent():
            #    # Only set the sample to first daughter in parent set, otherwise you will doublecount
            #    DS.set_application_mode(True)
            #    subDS = DS.daughters[0]
            #    while subDS.is_parent():
            #        subDS.set_application_mode(True)
            #        subDS.set_application_tree(tree)
            #        subDS = subDS.daughters[0]
            #else:
            #    DS.set_application_mode(True)
            #    DS.set_application_tree(tree)

        for DSName, DS in self.CH.dDatasets.iteritems():
            if DS.is_spectator: continue
            if "Convert" in self.dDSConf[DSName]:
                if not self.dDSConf[DSName]["Convert"] : continue

            if not "Combined" in DS.name: continue
            DS.convert(modes=["test"], quiet=True)

            if "Normalize" in self.dDSConf[DSName]:
                if self.dDSConf[DSName]["Normalize"] : DS.normalize()
            
        for DSName, DS in self.CH.dDatasets.iteritems():
            if DS.is_spectator: continue
            for part, sel in dSelCache[DSName].iteritems():
                setattr(DS, "sel_"+part, sel)
        return self.JO.applyModel(self.CH)


