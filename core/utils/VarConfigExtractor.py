from core.utils.logger import log
from core.dataset import DatasetBase, RecurrentDataset
import json

def variables_to_dict(dInputs, lOutputs):


    lInputsSequence = []
    lInputs = []

    for layer, InputDS in dInputs.iteritems():
        if not isinstance(InputDS, DatasetBase):
            log().fatal("Cant extract variable normalization from input layer {}! Input dataset is not of type DatasetBase".format(layer.name))
        if not InputDS.is_converted:
            log().fatal("Cant extract variable normalization from input layer {} if dataset is not converted!".format(layer.name))
            return None

        lVars = InputDS.var
        if InputDS.is_normalized:
            lMeans = InputDS.var_means
            lDev = InputDS.var_deviations
        else:
            lMeans = [1. for _ in range(0, len(lVars))]
            lDev = [1. for _ in range(0, len(lVars))]

        lVarConfig = []
        for i in range(0, len(lVars)):
            lVarConfig.append({ "name" : lVars[i],
                                "offset" : -1.0 * float(lMeans[i]),
                                "scale" : 1.0 / float(lDev[i]),
                })

        dInpConf = { "name" : layer.name,
                     "variables" : lVarConfig
        }

        if isinstance(InputDS, RecurrentDataset):
            lInputsSequence.append(dInpConf)
        else:
            lInputs.append(dInpConf)

    lOutNames = []
    for layer in lOutputs:
        layerConfig = layer.get_config()
        if "layer" in layerConfig:
            layerConfig = layerConfig["layer"]

        nOutputs = layerConfig["config"]["units"]
        lOutputName = ["type_{}".format(i) for i in range(0, nOutputs)]
        
        lOutNames.append({ "name" : layer.name,
                          "labels" : lOutputName,
                        })


    dConfig = {"input_sequences": lInputsSequence, "inputs" : lInputs, "outputs" : lOutNames}
    return dConfig