from core.utils.logger import log

class ConfigHandler:

    def __init__(self):
        
        self.inpFolder = ""
        self.mergeConfFile = ""
        self.mcWeightsFile = ""

        # save this to circumvent cyclic dependency on woker class
        self.modelOutput = ""

        # stores datasets and samples
        self.dDatasets = {}
        self.dSamples = {}
        self.dLabels = {}
        self.dWeights = {}
        self.model = None
        self.history = None
        self.varDict = None
        
        # saves configs of the sample preparations
        self.dDSConf = {}
        self.dSampleConf = {}
        self.dVarConf = {}
        self.dNNConf = {}
        self.dFitConf = {}        

        self.kFoldConf = ""
        
        pass

    def addAndConfigDataset(self, DS, DSName=""):
        if not DSName:
            DSName = DS.name

        if DSName in self.dDSConf:
            for attr, val in self.dDSConf[DSName].iteritems():

                if attr == "FromSample":
                    self.__checkSetAttr(DS, "sample", self.dSamples[val])  
                    continue   
                if attr == "FromVars":
                    self.__checkSetAttr(DS, "var", self.dVarConf[val])
                    continue   

                if attr in ["Normalize", "Convert"]:
                    # these special cases are treated by the worker
                    continue
                
                self.__checkSetAttr(DS, attr, val)

        self.addDataset(DS, DSName)


    def addDataset(self, DS, DSName=""):
        if not DSName:
            DSName = DS.name
        if DSName in self.dDatasets:
            log().error("Multiple datasets with same name registered: {}".format(DSName))
            raise Exception("Dataset name error")

        DS.initialize()
        self.dDatasets[DSName] = DS

    def getCheckPointName(self):
        if self.kFoldConf:
            return self.getModelOutput()+"model_weights.kFold_"+self.kFoldConf+".{epoch:02d}-{val_loss:.2f}.hdf5"
        else:
            return self.getModelOutput()+"model_weights.{epoch:02d}-{val_loss:.2f}.hdf5"

    def addAndConfigSample(self, S, SName=""):
        if not SName:
            SName = S.name

        if SName in self.dSampleConf:
            for attr, val in self.dSampleConf[SName].iteritems():                    
                self.__checkSetAttr(S, attr, val)

        self.__checkSetAttr(S, "file_path", self.inpFolder)
        self.__checkSetAttr(S, "mcweights_file", self.mcWeightsFile)
        self.__checkSetAttr(S, "mergeconf_file", self.mergeConfFile)
        
        self.addSample(S, SName)


    def addSample(self, S, SName=""):
        if not SName:
            SName = S.name

        if SName in self.dSamples:
            log().error("Multiple samples with same name registered: {}".format(SName))
            raise Exception("Sample name error")
        
        S.initialize()
        self.dSamples[SName] = S


    def addWeight(self, Weights, WeightsName=""):
        if not WeightsName:
            WeightsName = Weights.name

        if WeightsName in self.dWeights:
            log().error("Multiple weights with same name registered: {}".format(WeightsName))
            raise Exception("Weight name error")

        self.dWeights[WeightsName] = Weights


    def addLabel(self, Label, LabelName=""):
        if not LabelName:
            LabelName = Label.name

        if LabelName in self.dLabels:
            log().error("Multiple labels with same name registered: {}".format(LabelName))
            raise Exception("Label name error")

        Label.initialize()
        self.dLabels[LabelName] = Label


    def setModel(self, model):
        self.model = model


    def setFitHistory(self, history):
        self.history = history


    def setVarDict(self, varDict):
        self.varDict = varDict


    def __checkSetAttr(self, obj, attr, val):
        if not hasattr(obj, attr):
            log().error("Object of type {} with name {} has no attribute {} to configure".format(type(obj), obj.name, attr))
            log().error("Pleas check your job configuration yaml file!")
            return None

        #if bool(getattr(obj, attr)):
        #    log().warning("Arrtibute {} already set for {} with name {}".format(attr, str(obj), obj.name))
        #    log().warning("Will overwrite attribute!")

        setattr(obj, attr, val)

    def getDatasetConf(self):
        return self.dDSConf

    def getDataset(self, DSName):
        if not DSName in self.dDatasets:
            log().warning("ConfigHandler: Requested unknown dataset {}".format(DSName))
            return None

        return self.dDatasets[DSName]

    def getSampleConf(self):
        return self.dSampleConf

    def getSample(self, SampleName):
        if not SampleName in self.dSamples:
            log().warning("ConfigHandler: Requested unknown sample {}".format(SampleName))
            return None

        return self.dSamples[SampleName]

    def getLabel(self, LabelName):
        if not LabelName in self.dLabels:
            log().warning("ConfigHandler: Requested unknown labels {}".format(LabelName))
            return None

        return self.dLabels[LabelName]

    def getWeight(self, WeightName):
        if not WeightName in self.dWeights:
            log().warning("ConfigHandler: Requested unknown weights {}".format(WeightName))
            return None

        return self.dWeights[WeightName]

    def getNNConf(self, NNCName):
        if not NNCName in self.dNNConf:
            log().warning("ConfigHandler: Requested unknown network config {}".format(NNCName))
            return None

        return self.dNNConf[NNCName]

    def getFitConf(self, FitCName):
        if not FitCName in self.dFitConf:
            log().warning("ConfigHandler: Requested unknown network config {}".format(FitCName))
            return None

        return self.dFitConf[FitCName]

    def getModel(self):
        if not self.model:
            log().warning("ConfigHandler: Requested model but model has not been set yet")
            return None

        return self.model

    def getModelOutput(self):
        if not self.modelOutput.endswith("/"):
            self.modelOutput += "/"
        return self.modelOutput
