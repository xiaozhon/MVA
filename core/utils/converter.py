from root_numpy import tree2array
import numpy as np
from logger import log

class Converter(object):
    def __init__(self):
        pass


    def tree2numpyRecurrent(self, tree, variables, start, stop, steps, rec_steps, event_sel=None):
        npArray = tree2array(tree, branches=variables, selection=event_sel, start=start, stop=stop, step=steps)
        if npArray.size == 0:
            return npArray
        #log().warning("Currently flipping axis 2")
        vectorized_stack = np.vectorize(regularize_padded, otypes=[np.object])
        npArray = vectorized_stack(npArray, rec_steps)
        npArray = np.stack(npArray)
        npArray = np.swapaxes(npArray, 1, 2)[:,:rec_steps,:]
        
        return npArray

    def tree2numpy(self, tree, variables, start, stop, steps, event_sel=None):
        npArray = tree2array(tree, branches=variables, selection=event_sel, start=start, stop=stop, step=steps)
        if npArray.size == 0:
            return npArray
        regularize_arr = np.vectorize(regularize, otypes=[np.object])
        npArray = regularize_arr(npArray)
        npArray = np.vstack(npArray)
        
        return npArray

def regularize(arr):
    if type(arr) == float:
        arr = [arr]
    aRegulized = np.zeros(len(arr))
    for idx, entry in enumerate(arr):
        if isinstance(entry, np.ndarray):
            aRegulized[idx] = entry[0]
        else:
            aRegulized[idx] = entry
    return aRegulized

def regularize_padded(tup, steps, padding=0):
    ret = []
    for arr in tup:
        #arr = np.flip(arr)
        if len(arr) < steps:
            arr = np.append(arr, [padding for _ in range(0, steps-len(arr))])
        if len(arr) > steps:
            arr = arr[:steps]
        ret.append(arr)
    return ret

def stridetify(arr, stride_length, axis=0, pad_value=0):
    if axis == arr.ndim-1:
        log().error("Can not create strides along the last axis of an array!")
        return None

    ret_array = None

    shape = arr.shape
    new_shape = list(shape)
    new_shape[axis+1] = new_shape[axis+1] * stride_length
    new_shape[axis] = 1
    new_shape = tuple(new_shape)

    pad_shape = [(0,0) for _ in range(0, arr.ndim)]
    pad_shape[axis] = (0, stride_length-1)
    arr = np.pad(arr, pad_shape , "constant", constant_values=pad_value)

    for i in range(0, shape[axis]):
        tmp_arr = arr.take(indices=range(i, i+stride_length), axis=axis, mode="raise").reshape(new_shape)
        if type(ret_array) == type(None):
            ret_array = tmp_arr
        else:
            ret_array = np.append(ret_array, tmp_arr, axis=axis)

    return ret_array 
