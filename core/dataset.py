## modules
import numpy as np
import ROOT
import h5py
from array import array
#from array import array

from core.base import BaseClass
from core.utils.logger import log
from core.sample import Sample
from core.utils.converter import Converter
from collections import OrderedDict

import math

class DatasetBase(BaseClass):

    def __init__(self,
                name,
                var = None,
                sample = None,
                nevents_train = 0.,
                nevents_val   = 0.,
                nevents_test   = 0.,
                sel       = "1.",
                sel_train = "1.",
                sel_val   = "1.",
                sel_test   = "1.",
                application_mode = False,
                is_spectator = False,
                ):

        BaseClass.__init__(self, name)
        # initial setup
        self.name    = name
        self.var     = var
        self.sel     = sel
        self.sel_train     = sel_train
        self.sel_val       = sel_val
        self.sel_test      = sel_test
        self.nevents_train = nevents_train
        self.nevents_val   = nevents_val
        self.nevents_test  = nevents_test

        self.sample  = sample

        # internl states
        self.is_converted  = False
        self.is_persistent = False
        self.is_normalized = False

        # daughterize
        self.parents = []
        self.daughters = list()

        # fetch trees from sample
        #self.trees   = self._fetch_trees()
        # instead fetch last trees statistics
        self.tree_cache_train = []
        self.tree_cache_val   = []
        self.tree_cache_test  = []

        # variables set application mode and tree
        # to apply NN on
        self.application_mode = application_mode
        self.application_tree = None
        self.is_spectator = is_spectator

        # internal data config and holders
        self.name_train  = self.name.replace(" ", "_")+"_train"
        self.data_train  = None
        self.shape_train = None
        self.persistent_file_train = None

        self.name_val    = self.name.replace(" ", "_")+"_val"
        self.data_val    = None
        self.shape_val   = None
        self.persistent_file_val = None

        self.name_test   = self.name.replace(" ", "_")+"_test"
        self.data_test   = None
        self.shape_test   = None
        self.persistent_file_test = None

        self.var_means = None
        self.var_deviations = None

        # initialize converter
        self.converter = Converter()

# _________________________________________________________________________________________________
    def initialize(self):
        # config samples
        if type(self.sample) == type(None):
            self.sample = []
        elif type(self.sample) != type(list()):
            self.sample = [self.sample]

# _________________________________________________________________________________________________
    def get_mean():
        return self.var_means

# _________________________________________________________________________________________________
    def get_stddev():
        return self.var_deviations

# _________________________________________________________________________________________________
    def is_parent(self):
        """Check if sample has daughters"""
        return bool(self.daughters)

# _________________________________________________________________________________________________
    def is_daughter(self):
        """Check if sample has parents"""
        return bool(self.parents)

# _________________________________________________________________________________________________
    def _auxData(self):
        return {"var" : self.var, 
                "var_deviations" : self.var_deviations, 
                "var_means" : self.var_means, 
                "is_normalized" : self.is_normalized,
                "shape_train" : self.shape_train,
                "shape_val" : self.shape_val,
                "shape_test" : self.shape_test}


# _________________________________________________________________________________________________
    def _normalize(self):
        """Calculate mean and standard deviation to normalize input

        IMPORTANT: implementation must be provided by derived class.

        """
        log().warn("Class derived from DatasetBase must provide implementation of  _normalize()!")

# _________________________________________________________________________________________________
    def normalize(self):
        if not self.is_converted:
            log().fatal("Cant normalize dataset {} bevor conversion".format(self.name))

        if not self.var_means or not self.var_deviations:
            value_mask_train = self.get_mask(self.data_train)
            self.var_means, self.var_deviations = self._get_mean_stdDev(self.data_train, value_mask_train)

        self.is_normalized = True
        self._normalize()

# _________________________________________________________________________________________________
    def _convert(self):
        """Convert TTree to HDF5 format

        IMPORTANT: implementation must be provided by derived class.

        :param output: output folder to dump HDF5 file
        :type output: str
        """
        log().warn("Class derived from DatasetBase must provide implementation of _convert()!")
        return None

# _________________________________________________________________________________________________
    def convert(self, quiet=False, modes=["train", "val"]):
        if not quiet:
            log().info("Start converting dataset {}".format(self.name))
        if self.is_parent():
           self.convert_daughters(quiet, modes)

        else:
            [self._convert(mode) for mode in modes]

        for mode in modes:
            if type(getattr(self, "data_"+mode)) == type(None): continue
            setattr(self, "shape_"+mode, getattr(self, "data_"+mode).shape )

        self.is_converted = True
        self._cleanup_trees()

# _________________________________________________________________________________________________
    def get_mask(self, values):
        if type(values) == type(None):
            return None
        return self._get_mask(values)
        
# _________________________________________________________________________________________________
    def _get_mask(self, values):
        """Calculates the mask determine if a given event (and timestep) will be processed in the training
        (entry = 1) or not (entry = 0).

        IMPORTANT: implementation must be provided by derived class.

        :param values: numpy array of data labels
        :type output: np array
        """
        log().warn("Class derived from DatasetBase must provide implementation of _get_mask()!")
        return None
# _________________________________________________________________________________________________
    def convert_daughters(self, quiet=False, modes=["train", "val"]):
        lDaughtersConvert = []
        if self.application_mode:
            # in conversion mode only convert fist dataset to avoid doublecounting
            lDaughtersConvert = [self.daughters[0]]
        else:
            lDaughtersConvert = self.daughters

        for daughter in lDaughtersConvert:
            if not quiet:
                log().info("Converting daughter dataset {}".format(daughter.name))
            
            daughter.convert(True, modes)

            for mode in modes:
                if type( getattr(daughter, "data_"+mode) ) == type(None): continue
                
                if type( getattr(self, "data_"+mode) ) == type(None):
                    setattr(self, "data_"+mode ,getattr(daughter, "data_"+mode) )
                else:
                    setattr(self, "data_"+mode ,np.append(getattr(self, "data_"+mode), getattr(daughter, "data_"+mode), axis=0) )

                setattr(daughter, "shape_"+mode, getattr(daughter, "data_"+mode).shape)
                delattr(daughter, "data_"+mode)
                setattr(daughter, "data_"+mode, None)
                #daughter.is_converted = False

# _________________________________________________________________________________________________
    def add_daughter(self, daughter):
        if not isinstance(daughter, type(self)):
            log().fatal("Could not add daughter to Dataset since types are inconsistent! (Parrent {}, Daughter {})".format(type(self), type(daughter)))
            return

        #if daughter.is_parent():
        #    log().fatal("Dataset {} cant be parrent of parrent {}!".format(self.name, daughter.name))
        #    log().fatal("This feature is not yet implemented!")
        #    return
        if self.sample:
            log().fatal("Dataset {} holds a source sample!")
            log().error("Datasets holding samples cant be parrents!")
            return

        for prevDaughter in self.get_final_daughters():
            if prevDaughter == self:
                    continue
            
            if prevDaughter.name == daughter.name:
                log().error("Dataset cant hold daughters with same name {}!".format(daughter.name))
                return

            for finalDaughter in daughter.get_final_daughters():
                if prevDaughter.get_draw_expr()!= daughter.get_draw_expr():
                    log().error("Dataset {} cant have daughters with different variables!".format(self.name))
                    return

        daughter.parents.append(self)
        self.var = daughter.var
        self.daughters.append(daughter)

# _________________________________________________________________________________________________
    def get_final_daughters(self):
        if not self.is_parent(): return [self]
        daughters = []
        for d in self.daughters: 
            daughters+=d.get_final_daughters()
        return daughters

# _________________________________________________________________________________________________
    def _fetch_trees(self):
        if type(self.sample) == type(None):
            log().error("Can not fetch trees. Sample is none type")
            raise Exception("None type error")

        self.tree_cache_train = []
        self.tree_cache_val = []

        if self.application_mode:
            if self.application_tree: return [self.application_tree]
            else: return [] 

        trees = []
        for sample in self.sample:
            trees += sample.get_trees()

        return trees

# _________________________________________________________________________________________________
    def _cleanup_trees(self):
        if type(self.sample) == type(None):
            log().error("Can not fetch trees. Sample is none type")
            raise Exception("None type error")

        for sample in self.sample:
            sample.cleanup_trees()

# _________________________________________________________________________________________________
    def get_draw_expr(self):
        # specialized reimplementation from Converter!
        return self.var

# _________________________________________________________________________________________________
    def _construct_cache(self, tree, shape):
        cache = {}
        for weight in ["XSec", "FilterEff", "KFactor", "NumEvents"]:
            cache[weight] = getattr(tree, weight)
        cache["NumEventsPass"] = shape[0]
        cache["npShape"] = shape
        return cache

# _________________________________________________________________________________________________
    def _add_tree_to_cache(self, tree, shape, mode):
        cache = self._construct_cache(tree, shape)
        lCache = getattr(self, "tree_cache_"+mode) + [cache]
        setattr(self, "tree_cache_"+mode, lCache)

# _________________________________________________________________________________________________
    def set_application_mode(self, mode):
        assert isinstance(mode, bool), "Application mode accepts only boolean expression"
        if not self.application_mode and mode:
            log().info("Setting up dataset {} in application mode.".format(self.name))
        
        if self.is_parent():
            for daughter in self.daughters:
                daughter.set_application_mode(mode)

        self.application_mode = mode

# _________________________________________________________________________________________________
    def is_file_in_sample(self, fileName):
        isInSample = False
        for sample in self.sample:
            if sample.is_file_in_sample(fileName): isInSample = True

        return isInSample

# _________________________________________________________________________________________________
    def set_application_tree(self, tTree):
        # set tree to apply
        self.application_tree = tTree
        self.data_train  = None
        self.shape_train = None
        self.data_val  = None
        self.shape_val = None
        self.data_test  = None
        self.shape_test = None
        self.is_converted = False
        if self.is_parent():
            for daughter in self.daughters:
                daughter.set_application_tree(tTree)

# _________________________________________________________________________________________________
    def get_test_data(self):
        return self.data_test

# _________________________________________________________________________________________________
    def persistify(self, outFolder, auxOnly=False):
        BaseClass.persistify(self, outFolder, auxOnly)

        if self.is_parent():
            for daughter in self.daughters:
                daughter.persistify(outFolder, True)
        

# _________________________________________________________________________________________________
    def link_data(self, outFolder, auxOnly=False):
        BaseClass.link_data(self, outFolder, auxOnly)

        if self.is_parent():
            for daughter in self.daughters:
                daughter.link_data(outFolder, True)

# _________________________________________________________________________________________________
class RecurrentDataset(DatasetBase):

    def __init__(self,
                name,
                var = None,
                sample = None,
                rec_steps = None,
                nevents_train = 0.,
                nevents_val   = 0.,
                nevents_test   = 0.,
                sel     = "1.",
                sel_train = "1.",
                sel_val   = "1.",
                sel_test   = "1.",
                application_mode = False,
                is_spectator = False,
                ):
        DatasetBase.__init__(self, name=name,sample=sample,var=var, sel=sel, sel_train=sel_train, sel_val=sel_val, sel_test=sel_test, nevents_train=nevents_train, nevents_val=nevents_val, nevents_test=nevents_test, application_mode=application_mode, is_spectator=is_spectator)
        self.rec_steps = rec_steps

# _________________________________________________________________________________________________
    def initialize(self):
        DatasetBase.initialize(self)
        if not self.rec_steps:
            self.rec_steps = 10

# _________________________________________________________________________________________________
    # mode to convert: train, val, test
    def _convert(self, mode):
        # first clean previous converted data
        setattr(self, "data_"+mode, None)
        data_tot = None
        
        trees = self._fetch_trees()
        for tree in trees:
            npArray = self.converter.tree2numpyRecurrent(tree, self.var, start=0, stop=tree.GetEntries(), steps=1, rec_steps=self.rec_steps, event_sel="({})*({})".format(self.sel, getattr(self ,"sel_"+mode)))
            
            if not self.application_mode: self._add_tree_to_cache(tree, npArray.shape, mode)

            if npArray.shape[0] > getattr(self, "nevents_"+mode) and getattr(self, "nevents_"+mode) > 0:
                # if nevets selected for train/val/test chop of nevents from tree
                npArray = npArray[getattr(self, "nevents_"+mode):]

            if npArray.size > 0:
                if type(getattr(self, "data_"+mode)) == type(None):
                    # if data train/val/test is not yet set
                    setattr(self, "data_"+mode, npArray)
                else:
                    # append if already set
                    setattr(self, "data_"+mode, np.append( getattr(self, "data_"+mode, npArray, axis=0) ))

            del npArray

# _________________________________________________________________________________________________
    def _normalize(self):
        if isinstance(self.data_train, np.ndarray):
            value_mask_train = self.get_mask(self.data_train)
            self.data_train = ((self.data_train - self.var_means).T * value_mask_train.T).T/self.var_deviations
        if isinstance(self.data_val, np.ndarray):
            value_mask_val = self.get_mask(self.data_val)
            self.data_val = ((self.data_val - self.var_means).T * value_mask_val.T).T/self.var_deviations
        if isinstance(self.data_test, np.ndarray):
            value_mask_test = self.get_mask(self.data_test)
            self.data_test = ((self.data_test - self.var_means).T * value_mask_test.T).T/self.var_deviations

# _________________________________________________________________________________________________
    def _get_mask(self, values):
        # get boolean mask of all events where none of the variables (axis = 2)
        # is zero. then return as float
        value_mask = np.amax(values, axis=2) != 0
        return value_mask.astype(float)

# _________________________________________________________________________________________________
    def _get_nevents_from_mask(self, value_mask):
        return np.sum(value_mask)

# _________________________________________________________________________________________________
    def _get_mean_stdDev(self, values, value_mask ):
        # first transform everything from (nEvents, nTracks, nVars) to
        # (nEvents x nTracks, nVars) for performance reasons
        N_masked = self._get_nevents_from_mask(value_mask)
        means = np.sum(np.sum(values, axis=0), axis=0) / N_masked
        
        # stdDv = sqrt( 1/N * sum( (x_1 - mean)**2 ) )
        stdDev = (((values - means).T) * (np.transpose(value_mask))).T
        stdDev = np.sqrt(np.sum(np.sum(np.square(stdDev), axis=0), axis=0).astype(float))
        stdDev *= 1.0/math.sqrt(N_masked)

        return means, stdDev

# _________________________________________________________________________________________________
class Dataset(DatasetBase):

    def __init__(self,
                name,
                var = None,
                sample = None,
                nevents_train = 0.,
                nevents_val   = 0.,
                nevents_test   = 0.,
                sel     = "1.",
                sel_train = "1.",
                sel_val   = "1.",
                sel_test   = "1.",
                application_mode = False,
                is_spectator = False,
                ):
        DatasetBase.__init__(self, name=name,sample=sample,var=var, sel=sel, sel_train=sel_train, sel_val=sel_val, sel_test=sel_test, nevents_train=nevents_train, nevents_val=nevents_val, nevents_test=nevents_test, application_mode=application_mode, is_spectator=is_spectator)

# _________________________________________________________________________________________________
    # mode to convert: train, val, test
    def _convert(self, mode):
        # first clean previous converted data
        setattr(self, "data_"+mode, None)
        data_tot = None
        
        #print "Convert DS: "+self.name
        trees = self._fetch_trees()
        for tree in trees:
            #print "Tree: "+str(tree)
            #print "TreeEvents: "+str(tree.GetEntries())
            npArray = self.converter.tree2numpy(tree, self.var, start=0, stop=tree.GetEntries(), steps=1, event_sel="({})*({})".format(self.sel, getattr(self ,"sel_"+mode)))
            #print "NP array size: "+str(npArray.shape)
            #print "NP array: "+str(npArray)
            if not self.application_mode: self._add_tree_to_cache(tree, npArray.shape, mode)

            if npArray.shape[0] > getattr(self, "nevents_"+mode) and getattr(self, "nevents_"+mode) > 0:
                # if nevets selected for train/val/test chop of nevents from tree
                npArray = npArray[getattr(self, "nevents_"+mode):]

            if npArray.size > 0:
                if type(getattr(self, "data_"+mode)) == type(None):
                    # if data train/val/test is not yet set
                    setattr(self, "data_"+mode, npArray)
                else:
                    # append if already set
                    setattr(self, "data_"+mode, np.append( getattr(self, "data_"+mode), npArray, axis=0) )

            del npArray

# _________________________________________________________________________________________________
    def _normalize(self):
        if isinstance(self.data_train, np.ndarray):
            value_mask_train = self.get_mask(self.data_train)
            self.data_train = ((self.data_train - self.var_means).T * value_mask_train.T).T/self.var_deviations
        if isinstance(self.data_val, np.ndarray):
            value_mask_val = self.get_mask(self.data_val)
            self.data_val = ((self.data_val - self.var_means).T * value_mask_val.T).T/self.var_deviations
        if isinstance(self.data_test, np.ndarray):
            value_mask_test = self.get_mask(self.data_test)
            self.data_test = ((self.data_test - self.var_means).T * value_mask_test.T).T/self.var_deviations

# _________________________________________________________________________________________________
    def _get_mask(self, values):
        # get boolean mask of all events where none of the variables (axis = 1)
        # is zero. then return as float
        value_mask = np.amax(values, axis=1) != 0
        return value_mask.astype(float)

# _________________________________________________________________________________________________
    def _get_nevents_from_mask(self, value_mask):
        return np.sum(value_mask)

# _________________________________________________________________________________________________
    def _get_mean_stdDev(self, values, value_mask ):
        # first transform everything from (nEvents, nTracks, nVars) to
        # (nEvents x nTracks, nVars) for performance reasons
        N_masked = self._get_nevents_from_mask(value_mask)
        means = np.sum(values, axis=0) / N_masked
        
        # stdDv = sqrt( 1/N * sum( (x_1 - mean)**2 ) )
        stdDev = (((values - means).T) * (np.transpose(value_mask))).T
        stdDev = np.sqrt(np.sum(np.square(stdDev), axis=0).astype(float))
        stdDev *= 1.0/math.sqrt(N_masked)

        return means, stdDev