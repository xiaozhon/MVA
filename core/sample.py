from core.utils.logger import log
from ruamel import yaml
import glob, ROOT, sys

class Sample():

    def __init__(self,
                 name = None,
                 daughters = None,
                 proc = None,
                 file_path = None,
                 tree = None,
                 sub_dir = "Nominal",
                 mergeconf_file = None,
                 mcweights_file = None,
                 is_data = False
                 ):

        self.name = name
        self.isParent = False
        self.parent = None
        self.daughters = []

        self.proc = proc
        self.file_path = file_path

        self.tree = tree
        self.sub_dir = sub_dir
        self.mergeconf_file = mergeconf_file
        self.mcweights_file= mcweights_file

        self.is_data = is_data

        self.trees = []
        self.files = []

# _________________________________________________________________________________________________
    def initialize(self):
        # config daughters
        if self.daughters:
            for daughter in self.daughters:
                self.add_daughter(daughter)

        # fix file paths if nesecarry
        if self.file_path:
            if not self.file_path.endswith("/"): self.file_path += "/"

        #parse options
        self.mergeconf = self.__parse_yaml(self.mergeconf_file)
        self.mcweights = self.__parse_yaml(self.mcweights_file)

# _________________________________________________________________________________________________
    def get_parent(self):
        return self.parent

# _________________________________________________________________________________________________
    def is_parent(self):
        self.isParent = (len(self.daughters) != 0)
        return self.isParent
        
# _________________________________________________________________________________________________
    def add_daughter(self, daughter):
        if not isinstance(daughter, Sample):
            return 0
        if daughter.get_parent():
            log().warning("Daughter of sample {} already has parent. Multi-parenting not yet supported!".format(self.name))
            log().warning("In case this feature is needed, ask support for help ( or just DIY :) )".format(self.name))

        self.daughters.append(daughter)
        self.isParent = True
        daughter.parent = self
        return 1
        
# _________________________________________________________________________________________________
    def get_final_daughters(self):
        if not self.is_parent(): return [self]
        daughters = []
        for d in self.daughters: 
            daughters+=d.get_final_daughters()
        return daughters
        
# _________________________________________________________________________________________________
    def get_dsid_info_by_proc(self):
        dDSID = {}
        if self.is_parent():
            for daughter in self.get_final_daughters():
                dDSID.update(daughter.get_dsid_info_by_proc())
            return dDSID
            
        dProcOptions = self.mergeconf["Merge"]
        if not self.proc in dProcOptions and not self.is_data:
            log().error("Process {} not declared in plot option file!".format(self.proc))
            return 0
        if self.is_data:
            dDSID["data"] = {}
            dDSID["data"]["XSec"]      = 1.
            dDSID["data"]["FilterEff"] = 1.
            dDSID["data"]["KFactor"]   = 1.
            dDSID["data"]["NumEvents"] = 1.
            return dDSID

        lSubProcs = [proc for proc in dProcOptions[self.proc] if isinstance(proc, str) ]       
        for subProc in lSubProcs:
            for mcProcConf in self.mcweights.values():
                if mcProcConf["Process"] == subProc:
                    DSID = mcProcConf["DSID"] 
                    dDSID[DSID] = {}
                    dDSID[DSID]["XSec"]      = mcProcConf["XSec"]
                    dDSID[DSID]["FilterEff"] = mcProcConf["FilterEff"]
                    dDSID[DSID]["KFactor"]   = mcProcConf["KFactor"]
                    dDSID[DSID]["NumEvents"] = mcProcConf["NumEvents"]
        return dDSID
        
# _________________________________________________________________________________________________
    def fetch_files_by_proc(self):
        dDSID = {}
        lFiles = []
        if self.is_parent():
            for daughter in self.get_final_daughters():
                dDSID.update(daughter.fetch_files_by_proc())
            return dDSID

        dDSID = self.get_dsid_info_by_proc()
        for DSID in dDSID.keys():
            dDSID[DSID]["Files"] = glob.glob(self.file_path+"*{}*".format(DSID))

        return dDSID

# _________________________________________________________________________________________________
    def get_trees(self):
        if self.trees:
            return self.trees
        lTrees = []
        lFiles = []
        if self.is_parent():
            for daughter in self.get_final_daughters():
                lTrees += daughter.get_trees()
            return lTrees


        for DSID, conf in self.fetch_files_by_proc().iteritems():
            for file in conf["Files"]:
                f = ROOT.TFile.Open(file, "READ")

                treeName = ""
                if self.sub_dir:
                    treeName += self.sub_dir+"/"
                treeName += self.tree
                
                t = f.Get(treeName)
                t.XSec      = conf["XSec"]
                t.FilterEff = conf["FilterEff"]
                t.KFactor   = conf["KFactor"]
                t.NumEvents = conf["NumEvents"]
                lFiles.append(f)
                lTrees.append(t)
        self.files = lFiles
        self.trees = lTrees
        return lTrees
        
# _________________________________________________________________________________________________
    def cleanup_trees(self):
        #for tree in self.trees:
        #    tree.Delete()
        for file in self.files:
            file.Close()

        self.trees = []
        self.files = []

# _________________________________________________________________________________________________
    def __parse_yaml(self, yamlFile):
        dRet = None
        if not yamlFile:
            return None
        with open(yamlFile, "r") as stream:
            try:
                dRet = yaml.load(stream, Loader=yaml.SafeLoader)
            except:
                log().error("Sample {} can not open YAML file {}!".format(self.name, yamlFile))
                print(sys.exc_info()[0])
        return dRet

# _________________________________________________________________________________________________
    def is_file_in_sample(self, sFile):
        isInSample = False
        fileName  = sFile.split("/")[-1]
        for DSID, conf in self.fetch_files_by_proc().iteritems():
            for sampleFile in conf["Files"]:
                if fileName in sampleFile:  
                    isInSample = True
                    
        return isInSample