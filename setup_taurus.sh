module load binutils/2.30-GCCcore-7.3.0
module load Tkinter/2.7.15-foss-2018b-Python-2.7.15

MVA_GPU_MODE=0
if [[ $SLURM_JOB_PARTITION =~ "gpu" ]]
then
    echo "RUNNING IN GPU MODE!"
    MVA_GPU_MODE=1
    module load CUDA/10.0.130
    module load cuDNN/7.4.2.24-CUDA-10.0.130
fi

echo "========================================"
echo "Hello mu name is: "`hostname`
echo "Start setting up myself"
echo "========================================"
source setup_mva.sh