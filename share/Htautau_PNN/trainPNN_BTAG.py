from core import dataset
from core.sample import Sample
from core.dataset import Dataset
from core.labels import DatasetClassLables
from core.weights import DatasetBalance

import keras as keras
from keras.callbacks import Callback
from keras import backend as K
from keras.models import Model
from keras.layers import Dense, Lambda, Input
from keras.callbacks import ReduceLROnPlateau, ModelCheckpoint
from keras.optimizers import Adam
from keras.losses import mse, binary_crossentropy
from keras.utils import plot_model

import numpy as np

def defineSamples(ConfHandler):
    #samples configured by JobConf.yml
    lMasses = ["200", "250", "300", "350", "400", "500", "600", "700", "800", "1000", "1200", "1500", "2000", "2500"]

    SignalDS = Dataset("SignalDS")
    
    for mass in lMasses:
        ConfHandler.addAndConfigSample( Sample(name = "ggH{}Sample".format(mass)) )
        ConfHandler.addAndConfigDataset( Dataset("H{}_DS".format(mass)) )
        SignalDS.add_daughter(ConfHandler.getDataset("H{}_DS".format(mass)))
    
    ConfHandler.addAndConfigDataset(SignalDS)

    MJSample = Sample(name="MultijetSample")
    ConfHandler.addAndConfigSample(MJSample)
    # dirty hack to force using data
    MJSample.file_path = MJSample.file_path.replace("fac_TAUS", "data_merged")
    
    MultijetDS = Dataset("MultijetDS")
    ConfHandler.addAndConfigDataset(MultijetDS)

    ConfHandler.addAndConfigSample( Sample(name="TopSample") )
    TopDS = Dataset("TopDS")
    ConfHandler.addAndConfigDataset( TopDS )

    BackgroundDS = Dataset("BackgroundDS")
    BackgroundDS.add_daughter(TopDS)
    BackgroundDS.add_daughter(MultijetDS)
    ConfHandler.addAndConfigDataset(BackgroundDS)

    CombinedDS = Dataset("CombinedDS")
    CombinedDS.add_daughter(BackgroundDS)
    CombinedDS.add_daughter(SignalDS)
    ConfHandler.addAndConfigDataset(CombinedDS)


    #ConfHandler.addAndConfigSample( Sample(name = "TopSample") )
    #ConfHandler.addAndConfigDataset( Dataset("Top_DS") )
    

def defineLabels(ConfHandler):
    LabelsDS = DatasetClassLables(name="ClassLabels", dataset_keys=["SignalDS", "BackgroundDS"], do_binary=True)(ConfHandler.getDataset("CombinedDS"))
    ConfHandler.addLabel(LabelsDS)
    #label = "({0} == 1)*0 + ({0} == 2)*1 + ({0} == 3)*2 + ({0} == 0 || {0} >= 4)*3".format("TruthType")
    #LabelsDS= RecurrentClassLabels("LabelsCombined", label)(ConfHandler.getDataset("CombinedDS"))

def defineWeights(ConfHandler):
    DSBalance = DatasetBalance("DSbalance", ds_keys_weights={"MultijetDS" : 8, "TopDS" : 8})(ConfHandler.getDataset("CombinedDS"))
    ConfHandler.addWeight(DSBalance)


def defineModel(ConfHandler):
    ## Note we add masses later
    input_shape = ConfHandler.getDataset("CombinedDS").get_train_data().shape[1] + 1

    hidden_dim = ConfHandler.getNNConf("HiddenSize")
    hidden_layers = ConfHandler.getNNConf("HiddenLayers")

    lLayerSize = [int(hidden_dim/i) for i in range(1, hidden_layers+1) if int(hidden_dim/i) > 2] 

    # VAE model = encoder + decoder
    # build encoder model
    inputs = Input(shape=(input_shape,))

    x_prev = inputs
    for layerSize in lLayerSize:
        x = Dense(layerSize, activation='relu')(x_prev)
        x_prev = x
    
    outputs = Dense(1, activation='sigmoid')(x_prev)
        
    # instantiate VAE model
    model = Model(inputs, outputs, name='mlp')

    plot_model(model, to_file=ConfHandler.getModelOutput()+'vae_{}.png'.format(ConfHandler.kFoldConf), show_shapes=True)

    model.compile(optimizer="adam", loss="binary_crossentropy", metrics=['accuracy'], weighted_metrics=['accuracy'])

    ConfHandler.setModel(model)


def fitModel(ConfHandler):
    
    dataTrain = augmentMass(ConfHandler.getDataset("CombinedDS"), "train")
    dataVal   = augmentMass(ConfHandler.getDataset("CombinedDS"), "val")

    labelsTrain = ConfHandler.getLabel("ClassLabels").get_train_data()
    labelsVal   = ConfHandler.getLabel("ClassLabels").get_val_data()

    weightTrain  = ConfHandler.getWeight("DSbalance").get_train_data()
    weightVal    = ConfHandler.getWeight("DSbalance").get_val_data()

    #print "===================================================================="
    #offset = 0
    #for subDS in ConfHandler.getDataset("CombinedDS").get_final_daughters():
    #    print " {} shape: {}".format(subDS.name, subDS.shape_train)
    #    print dataTrain[offset:offset+subDS.shape_train[0], :10]
    #    print weightTrain[offset:offset+10]
    #    print labelsTrain[offset:offset+10]
    #    offset += subDS.shape_train[0]
    #print "===================================================================="
    #raw_input()
    #print dataVal
    
    epochs = ConfHandler.getFitConf("Epochs")
    batch_size = ConfHandler.getFitConf("BatchSize")
    model = ConfHandler.getModel()

    CBList = list()
    CBList.append(ModelCheckpoint(ConfHandler.getCheckPointName(), save_weights_only=True) )
    CBList.append(ReduceLROnPlateau(monitor='val_loss', factor=0.3, patience=3, min_lr=0.0001, verbose=1))
    history = model.fit_generator(augmentMassGenerator(dataTrain, labelsTrain, weightTrain, batch_size=batch_size, aug_masses=getSignalMassDict().values()), 
                                  steps_per_epoch = int(dataTrain.shape[0] / batch_size),
                                  epochs=epochs, 
                                  validation_data=(dataVal, labelsVal, weightVal), 
                                  callbacks=CBList,
                                  verbose=2)

    ConfHandler.setFitHistory(history)

def applyModel(ConfHandler):

    dataTest = ConfHandler.getDataset("CombinedDS").get_test_data()
    if not isinstance(dataTest, np.ndarray):
        return None

    massArr = np.zeros( (dataTest.shape[0], 1) )
    arg_dataTest = np.append(dataTest, massArr, axis=1)

    model = ConfHandler.getModel()
    dOutPred = {}

    for sigName, sigMass in getSignalMassDict().iteritems():
        arg_dataTest[:,-1] = sigMass
        pred = model.predict(arg_dataTest)
        dOutPred["score_"+sigName.replace("DS", "NN")] = pred

    return dOutPred
    

def getSignalMassDict():
    dSigMasses = {"H200_DS" :  0.2,
                  "H250_DS" :  0.25,
                  "H300_DS" :  0.3,
                  "H350_DS" :  0.35,
                  "H400_DS" :  0.4,
                  "H500_DS" :  0.5,
                  "H600_DS" :  0.6,
                  "H700_DS" :  0.7,
                  "H800_DS" :  0.8,
                  "H1000_DS" : 1.,
                  "H1200_DS" : 1.2,
                  "H1500_DS" : 1.5,
                  "H2000_DS" : 2.,
                  "H2500_DS" : 2.5}
    return dSigMasses

def augmentMassGenerator(data, labels, weights, batch_size, aug_masses, shuffle=True):
    if shuffle:
        p = np.random.permutation(data.shape[0])
        data = data[p]
        labels = labels[p]
        weights = weights[p]

    startIdx = 0
    while True:
        endIdx = startIdx+batch_size
        
        data_aug = np.copy(data[startIdx:endIdx])
        mass = np.random.choice(aug_masses, data_aug.shape[0])
        np.place(data_aug, data_aug==-1111, mass)

        yield (data_aug, labels[startIdx:endIdx], weights[startIdx:endIdx])
        startIdx = endIdx if endIdx+batch_size<data.shape[0] else 0


def augmentMass(DS, mode="train"):
    lSubDS = DS.get_final_daughters()
    data = getattr(DS, "get_{}_data".format(mode))()

    massArr = np.ones((data.shape[0],1))
    
    startIdx = 0
    dSigMasses = getSignalMassDict()
    for subDS in lSubDS:
        mass = -1111
        endIdx = getattr(subDS, "shape_{}".format(mode))[0]
        
        if subDS.name in dSigMasses:
            mass = dSigMasses[subDS.name]
        elif mode!="train":
            mass = np.random.choice(dSigMasses.values(), (endIdx, 1))
        
        massArr[startIdx:startIdx+endIdx,:] *= mass
        startIdx += endIdx

    return np.append(data, massArr, axis=1)